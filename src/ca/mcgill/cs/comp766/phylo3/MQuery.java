package ca.mcgill.cs.comp766.phylo3;

/**
 * Created by Richboy on 22/09/2015.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MQuery {
    private boolean isConnected;
    private boolean isFullyConnected;
    private PreparedStatement ps;
    private Exception exception;
    private Connection con;
    private String queryString;

    public MQuery(String queryString, boolean continueStatus) {
        this.queryString = queryString;
        isFullyConnected  = false;
        isConnected = false;
        try{
            try{
                Class.forName(Util.MYSQL_DRIVER).newInstance();
            }
            catch(Exception e){
                Class.forName(Util.MYSQL_DRIVER);
            }
            con = DriverManager.getConnection(Util.MYSQL_DB_URL_PREFIX + Util.MYSQL_DB_HOST + ":" + Util.MYSQL_DB_PORT + "/" + Util.MYSQL_DB_NAME, Util.MYSQL_DB_USERNAME, Util.MYSQL_DB_PASSWORD);

            isConnected = true;

            if( continueStatus )
                nextPhase();
        }
        catch(Exception e){
            isConnected = false;
            exception = e;
        }
    }

    public MQuery( String queryString ){
        this(queryString, true);
    }

    public void nextPhase(){
        try{
            ps = con.prepareStatement(queryString, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            isFullyConnected = true;
        }
        catch( Exception e ){
            exception = e;
            isFullyConnected = false;
        }
    }

    public void setQuery(String queryString){
        setQueryString(queryString);
        nextPhase();
    }

    public String getQueryString(){
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public boolean isConnected(){
        return isConnected;
    }

    public boolean isFullyConnected(){
        return isFullyConnected;
    }

    public PreparedStatement getPS(){
        return ps;
    }

    public Exception getException(){
        return exception;
    }

    public Connection getCon(){
        return con;
    }

    public void disconnect(){
        try{
            ps.close();
        }
        catch(Exception e){}
        try{
            con.close();
        }
        catch(Exception e){}
    }
}