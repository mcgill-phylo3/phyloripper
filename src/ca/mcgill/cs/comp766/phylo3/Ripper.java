package ca.mcgill.cs.comp766.phylo3;

import org.json.me.JSONObject;

import ca.mcgill.cs.comp766.phylo3.Util.ErrorContext;

public abstract class Ripper {
	protected boolean isDone;
	protected JSONObject data = new JSONObject();
	protected Util.ErrorContext context;
	
	public abstract ErrorContext rip();
	public JSONObject getData(){
		return data;
	}
}
