package ca.mcgill.cs.comp766.phylo3;

import java.sql.ResultSet;

import org.json.me.JSONObject;
import org.json.me.util.XML;
import org.json.me.util.XMLTokener;

import ca.mcgill.cs.comp766.phylo3.Util.ErrorContext;

public class DiseaseRipper extends Ripper{
	private static DiseaseRipper diseaseRipper;
	private static final String keyPrefix = "disease";
	private int count;
	
	private DiseaseRipper(){
		count = 0;
	}
	
	public static DiseaseRipper getInstance(){
		if( diseaseRipper == null )
			diseaseRipper = new DiseaseRipper();
		return diseaseRipper;
	}

	@Override
	public ErrorContext rip() {
		if( isDone )
			return context;
		
		String id;
		
		try{
			MQuery query = new MQuery("SELECT disease_link, disease_category FROM levels WHERE disease_link IS NOT NULL AND disease_category IS NOT NULL GROUP BY disease_link, disease_category");
			if( query.isFullyConnected() ){
				try{
					ResultSet rs = query.getPS().executeQuery();
					
					while(rs.next()){
						id = keyPrefix + (count++);
						
						JSONObject object = new JSONObject();
						object.put("id", id);//may not be required
						object.put("name", rs.getString("disease_link"));
						object.put("category", rs.getString("disease_category"));
						object.put("url", "");
						object.put("description", "");
						
						data.put(id, object);
					}
					
					rs.close();
					
					context = new ErrorContext(true, "Successful!");
				}
				catch(Exception e){
					e.printStackTrace();
					context = new ErrorContext(false, e.getMessage());
				}
			}
			else{
				query.getException().printStackTrace();
				context = new ErrorContext(false, query.getException().getMessage());
			}
			
			return context;
		}
		finally{
			isDone = true;
		}
	}

}
