package ca.mcgill.cs.comp766.phylo3;

public class Util {
	public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    public static final String MYSQL_DB_URL_PREFIX = "jdbc:mysql://";
    public static String MYSQL_DB_HOST = "localhost";// = "127.0.0.1";
    public static String MYSQL_DB_PORT = "3306";
    public static String MYSQL_TEST_DB_NAME = "mysql";
    public static String MYSQL_DB_NAME = "phylo";
    public static final String MYSQL_DB_USERNAME = "root";
    public static final String MYSQL_DB_PASSWORD = "";
	
	public static class ErrorContext{
		private boolean isSuccessful;
		private String message;
		private String title;
		
		public ErrorContext(boolean state, String mess){
			this(state, mess, null);
		}
		public ErrorContext(boolean state, String mess, String title){
			isSuccessful = state;
			message = mess;
			this.title = title;
		}
		public boolean isSuccessful(){
			return isSuccessful;
		}
		public boolean hasTitle(){
			return title != null;
		}
		public String getMessage(){
			return message;
		}
		public String getTitle(){
			return title;
		}
	}
}
