package ca.mcgill.cs.comp766.phylo3;

import java.util.Enumeration;

import org.json.me.JSONObject;

import ca.mcgill.cs.comp766.phylo3.Util.ErrorContext;

public class GameRipper extends Ripper{
	private static GameRipper gameRipper;
	private static int preferredGameSequenceLength = 21;
	private int sequenceLength;
	private static final String keyPrefix = "game";
	
	private GameRipper(){}
	
	public static GameRipper getInstance(){
		if( gameRipper == null )
			gameRipper = new GameRipper();
		return gameRipper;
	}

	@Override
	public ErrorContext rip() {
		if( isDone )
			return context;
		
		String id;
		int count = 0;
		
		try{
			PuzzleRipper.getInstance().rip();
			JSONObject puzzles = PuzzleRipper.getInstance().getData();
			
			try{
				Enumeration<String> keys = puzzles.keys();
				String puzzleKey;
				JSONObject puzzle;
				
				while(keys.hasMoreElements()){
					puzzleKey = keys.nextElement();
					puzzle = puzzles.getJSONObject(puzzleKey);
					
					//Split each puzzle into bits for the games
					int puzzleSequenceLength = puzzle.getJSONObject("data").getJSONArray("sequences").getString(0).length();
					sequenceLength = Math.min(puzzleSequenceLength, preferredGameSequenceLength);
					
					JSONObject game;
					
					int numberOfGames = (int) Math.ceil(puzzleSequenceLength / (double) sequenceLength);
					//System.out.println(numberOfGames);
					
					for( int i = 0; i < numberOfGames; i++ ){
						game = new JSONObject();
						
						id = keyPrefix + (count++);
						game.put("id", id);
						game.put("puzzle", puzzleKey);
						game.put("playCount", 0);
						game.put("successRate", 0);
						game.put("bestScore", 0);
						game.put("originalScore", 0);
						game.put("offset", i * sequenceLength);
						
						if( i + 1 == numberOfGames && puzzleSequenceLength % sequenceLength != 0 ){//the last Game in the puzzle.
							game.put("length", puzzleSequenceLength - (i * sequenceLength));
							System.out.println(puzzleKey + " - " + (puzzleSequenceLength - (i * sequenceLength)));
						}
						else
							game.put("length", sequenceLength);
						
						
						
						data.put(id, game);
					}
				}
				
				context = new ErrorContext(true, "Successful!");
			}
			catch(Exception e){
				e.printStackTrace();
				context = new ErrorContext(false, e.getMessage());
			}
			
			return context;
		}
		finally{
			isDone = true;
		}
	}

}
