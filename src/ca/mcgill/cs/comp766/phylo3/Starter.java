package ca.mcgill.cs.comp766.phylo3;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

import org.json.me.JSONObject;

public class Starter {
	private static final String fileName = "phylo_firebase_database.json";
	
	public static void main(String[] args){
		try{
			JSONObject pack = new JSONObject();
			
			DiseaseRipper.getInstance().rip();
			PuzzleRipper.getInstance().rip();
			GameRipper.getInstance().rip();
			
			pack.put("diseases", DiseaseRipper.getInstance().getData());
			pack.put("puzzles", PuzzleRipper.getInstance().getData());
			pack.put("games", GameRipper.getInstance().getData());
			
			Files.write(new File("C:\\tmp", fileName).toPath(), pack.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println("\n\nDONE!!!");
	}
}
