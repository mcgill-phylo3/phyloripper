package ca.mcgill.cs.comp766.phylo3;

import java.io.StringReader;
import java.sql.ResultSet;
import java.util.Enumeration;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.me.JSONArray;
import org.json.me.JSONObject;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import ca.mcgill.cs.comp766.phylo3.Util.ErrorContext;

public class PuzzleRipper extends Ripper{
	private static PuzzleRipper puzzleRipper;
	private static final String originalTable = "levels";
	private static final String keyPrefix = "puzzle";
	private static final int limit = 50;
	
	private PuzzleRipper(){
	}
	
	public static PuzzleRipper getInstance(){
		if( puzzleRipper == null )
			puzzleRipper = new PuzzleRipper();
		return puzzleRipper;
	}

	@Override
	public ErrorContext rip() {
		if( isDone )
			return context;
		
		String id;
		int count = 0;
		
		try{
			MQuery query = new MQuery("SELECT * FROM levels");
			if( query.isFullyConnected() ){
				try{
					ResultSet rs = query.getPS().executeQuery();
					
					while(rs.next()){
						id = keyPrefix + rs.getInt("level_id");
						
						JSONObject object = new JSONObject();
						object.put("id", id);//may not be required
						object.put("disease", findDiseaseID(rs.getString("disease_link"), rs.getString("disease_category")));
						object.put("data", convertLevelXMLToJSON(rs.getString("level_xml")));
						
						if( !object.getString("disease").trim().isEmpty() ){
							count++;
							data.put(id, object);
							
							if( count == limit )
								break;
						}
					}
					
					rs.close();
					
					context = new ErrorContext(true, "Successful!");
				}
				catch(Exception e){
					e.printStackTrace();
					context = new ErrorContext(false, e.getMessage());
				}
			}
			else{
				query.getException().printStackTrace();
				context = new ErrorContext(false, query.getException().getMessage());
			}
			
			return context;
		}
		finally{
			isDone = true;
		}
	}
	
	private String findDiseaseID(String name, String category){
		String diseaseID = "";
		
		DiseaseRipper.getInstance().rip();//ask the Ripper to rip in case is has not done this before
		JSONObject diseaseObj = DiseaseRipper.getInstance().getData();
		
		try{
			Enumeration<String> keys = diseaseObj.keys();
			String key;
			while( keys.hasMoreElements() ){
				key = keys.nextElement();
				JSONObject object = diseaseObj.getJSONObject(key);
				
				if( name.equals(object.getString("name")) && category.equals(object.getString("category")) ){
					diseaseID = key;
					break;
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return diseaseID;
	}
	
	private JSONObject convertLevelXMLToJSON(String xml){
		JSONObject obj = new JSONObject();
		
		try{
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		    InputSource is = new InputSource();
		    is.setCharacterStream(new StringReader(xml));
		    
		    Document doc = db.parse(is);
		    NodeList nodes = doc.getElementsByTagName("sequence");
		    
		    JSONArray sequences = new JSONArray();
		    
		    for(int i = 0; i < nodes.getLength(); i++){
		    	Element element = (Element) nodes.item(i);
		    	sequences.put(getCharacterDataFromElement(element));
		    }
		    
		    
		    obj.put("sequences", sequences);
		    obj.put("tree", getCharacterDataFromElement(doc.getElementsByTagName("tree").item(0)));
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return obj;
	}
	
	private String getCharacterDataFromElement(Node e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}
}
